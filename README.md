# Kotlin Delegation Sample

### Things to run the app locally
#### Clone repository
```
git clone https://gitlab.com/hendisantika/kotlin-delegation-sample.git
```

#### Go the folder
```
cd kotlin-delegation-sample
```

### Run the app
```
gradle clean bootRun --info
```

### Screen shot

#### Home page

![Home Page](img/home.png "Home Page")

#### Add New Family Data

![Add New Family Data](img/add.png "Add New Family Data")

#### List All families data

![List All families data](img/list.png "List All families data")