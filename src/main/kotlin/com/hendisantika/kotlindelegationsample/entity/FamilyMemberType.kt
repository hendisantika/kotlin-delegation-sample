package com.hendisantika.kotlindelegationsample.entity

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-delegation-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-04
 * Time: 08:14
 */
enum class FamilyMemberType {
    Father, Mother, Daughter, Son
}