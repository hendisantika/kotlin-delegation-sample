package com.hendisantika.kotlindelegationsample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinDelegationSampleApplication

fun main(args: Array<String>) {
    runApplication<KotlinDelegationSampleApplication>(*args)
}
