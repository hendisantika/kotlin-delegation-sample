package com.hendisantika.kotlindelegationsample.controller

import com.hendisantika.kotlindelegationsample.entity.Family
import com.hendisantika.kotlindelegationsample.service.FamilyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import javax.validation.Valid

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-delegation-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-04
 * Time: 08:21
 */
@Controller
@RequestMapping("/")
class IndexController(
        @field: Autowired
        val familyService: FamilyService) {

    @ModelAttribute("konohaFamily")
    fun fetchFamily() = familyService.findAll()

    @ModelAttribute("family")
    fun fetchFamilies() = Family()

    @GetMapping
    fun doGet() = "index"

    @PostMapping
    fun doPost(@Valid family: Family, bindingResult: BindingResult, model: Model): String {
        var entity = family

        if (!bindingResult.hasErrors()) {
            familyService.save(family)
            entity = Family()
        }

        //Notice the use of extension functions to keep the code concise
        model.addFamily(entity)
        model.addKonohaFamily()

        return "index"
    }

    //Some private extension functions which tend to be really useful in Spring MVC
    private fun Model.addKonohaFamily() {
        addAttribute("konohaFamily", familyService.findAll())
    }

    private fun Model.addFamily(family: Family = Family()) {
        addAttribute("family", family)
    }
}