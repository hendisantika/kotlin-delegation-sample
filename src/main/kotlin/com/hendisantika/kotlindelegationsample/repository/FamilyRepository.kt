package com.hendisantika.kotlindelegationsample.repository

import com.hendisantika.kotlindelegationsample.entity.Family
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-delegation-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-04
 * Time: 08:17
 */
interface FamilyRepository : JpaRepository<Family, Long>